package backend;

import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.util.*;

public class CoinFlip extends Tab {
	private int money;
	private TextField betAmount;
	private Text betResult;
	private Button heads;
	private Button tails;
	
	public CoinFlip() {
		super("Flip");
		this.money = 100;
		setStage(new Text("Welcome To The Game!"));
	}
	
	public void setStage(Text result) {
		GridPane grid1 = new GridPane();
		
		Text tf1 = new Text("Money Available: ");
		Text tf2 = new Text(this.money + "");
		
		grid1.add(tf1, 0, 0);
		grid1.add(tf2, 1, 0);
	
		betAmount = new TextField();
		grid1.add(betAmount, 0, 10);
		
		betResult = result;
		grid1.add(betResult, 0, 15);
		
		heads = new Button("HEADS");
		grid1.add(heads, 0, 5);
		heads.setOnAction(buttonHandler);
		
		tails = new Button("TAILS");
		grid1.add(tails, 1, 5);
		tails.setOnAction(buttonHandler);
		
		grid1.setHgap(10);
		grid1.setVgap(5);
		grid1.setStyle("-fx-font: 20 arial;");
		
		this.setContent(grid1);
	}
	
	EventHandler<ActionEvent> buttonHandler = new EventHandler<ActionEvent>() {
	    public void handle(ActionEvent event) {
	    	if(event.getSource() == heads) {
	    		placeBet("heads");
	    	}else if(event.getSource() == tails){
	    		placeBet("tails");
	    	}
	    }
	};
	
	// heads == 0 
	// Tails == 1
	public int placeBet(String bet) {
		Random r1 = new Random();
		int randomInt = r1.nextInt(2);
		
		if (this.betAmount.getText().trim().isEmpty()){
			this.betResult = new Text("Bet placed is invalid");
			refresh(this.betResult);
			return 0;
		}else if ((Integer.parseInt(betAmount.getText()) > this.money)) {
			this.betResult = new Text("Unavailable Funds For Bet");
			refresh(this.betResult);
			return 0;
    	}else {
		
		if(bet.equals("heads") && randomInt == 0) {
			this.betResult = new Text("You Win!!!");
			this.money += Integer.parseInt(betAmount.getText());
		}else if(bet.equals("tails") && randomInt == 1) {
			this.betResult = new Text("You Win!!!");
			this.money += Integer.parseInt(betAmount.getText());
		}else {
			this.betResult = new Text("You Lose");
			this.money -= Integer.parseInt(betAmount.getText());
		}
    	}
		refresh(this.betResult);
		return 1;
	}
	
	public void refresh(Text result) {
		setStage(this.betResult);
	}

}






