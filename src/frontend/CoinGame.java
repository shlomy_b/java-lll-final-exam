package frontend;
import backend.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class CoinGame extends Application {
	public void start(Stage stage) {
		
		Group root = new Group();
		TabPane mainPane = new TabPane();
		
		CoinFlip game = new CoinFlip();
		mainPane.getTabs().add(game);
		
		root.getChildren().add(mainPane);	

		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.CORNFLOWERBLUE);

		stage.setTitle("Spelling Bee"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	  public static void main(String[] args) {
	      Application.launch(args);
	  }
}
