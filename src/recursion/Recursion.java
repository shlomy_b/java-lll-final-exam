package recursion;


public class Recursion {
	private int counter;

	public Recursion() {
		this.counter = 0;
	}
	public static void main(String[] args) {
		Recursion r1 = new Recursion();
		
		r1.foo(5);
		int[] arr1 = {1,2,3,4,5,6, 20};
		int n = 2;
		//System.out.println(recursiveCount(arr1,n));
		
	}
	
	public static int recursiveCount(int[] numbers, int n) {
		int counter = 0;
		
		  if (n <= 0) { 
			  if((n % 1 == 0 && (numbers[n] % 1 >= n)|| numbers[n] > 20)) {
				counter++;  
			  }else {
				  return counter;     
		  }
			  return 0; 
		  }
	        return (recursiveCount(numbers, n - 1) + numbers[n - 1]); 
	    } 

	
	 public int foo(int n) {
		 //this.counter = 0;
		 //System.out.println("Called by main ------ main");
		 
         if (n <= 0) {
                return Math.abs(n);
         }

         int x = foo(n-2);
         System.out.println("N Value" + n);
         //System.out.println("Called by n-2 ------ x"); // x called 20 times

         int y = foo(n-2);
         System.out.println("Called by n-2 ------ y"); // y called 21 times
         
         int z = foo(n-1);
         System.out.println("Called by n-1 --------- z"); // z called 21 times
         
         
         // total foo called 64 times
         
         System.out.println("x: " + x);
         System.out.println("y: " + y);
         System.out.println("z: " + z);
         System.out.println("total: " + (x + y + z));
         //System.out.println("");
         return x+ y + z;
         //+ y + z;
  }
	 /*
	    public static int food(int n) {

            if (n <= 0) {

                   return Math.abs(n);

            }

           

            int x = food(n-2);

            int y = food(n-2);

            int z = food(n-1);

            return x + y + z;

     }
     */
}
